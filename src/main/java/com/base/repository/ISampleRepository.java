/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.base.repository;

import com.base.models.Sample;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author kirill
 */
public interface ISampleRepository extends JpaRepository<Sample, Long>
{
    
}
